package com.ssm.order.controller;

import com.ssm.order.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by nanJunYu on 2016/7/15..
 */

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;


    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    private Object findAllUser() {
        return userService.findAll();
    }


}
