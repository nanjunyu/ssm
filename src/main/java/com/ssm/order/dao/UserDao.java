package com.ssm.order.dao;

import com.ssm.order.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by nanJunYu on 2016/7/15.
 */

@Repository
public interface UserDao {
    List<User> findAllUser();
}
