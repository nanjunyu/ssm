package com.ssm.order.service;

import com.ssm.order.model.User;

import java.util.List;

/**
 * Created by nanJunYu on 2016/7/15.
 */
public interface UserService {

    List<User> findAll();

}
