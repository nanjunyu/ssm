package com.ssm.order.service.impl;

import com.ssm.order.dao.UserDao;
import com.ssm.order.model.User;
import com.ssm.order.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by nanJunYu on 2016/7/15.
 */

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    public List<User> findAll() {
        return userDao.findAllUser();
    }
}
